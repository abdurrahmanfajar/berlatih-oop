<?php

require_once('Animal.php');
require_once('Frog.php');
require_once('Ape.php');

$sheep = new Animal("Shaun");
echo "Name : " . $sheep->name . "<br>"; 
echo "Legs : " . $sheep->legs . "<br>";
echo "Cold Blooded : " . $sheep->cold_blooded . "<br><br>";

$frog = new Frog("Buduk");
echo "Name : " . $frog->name . "<br>"; 
echo "Legs : " . $frog->legs . "<br>";
echo "Cold Blooded : " . $frog->cold_blooded . "<br>";
$frog->lompat("Hop Hop");

$Ape = new Ape("Kera Sakti");
echo "Name : " . $Ape->name . "<br>"; 
echo "Legs : " . $Ape->legs . "<br>";
echo "Cold Blooded : " . $Ape->cold_blooded . "<br>";
$Ape->teriak("Auooo");

?>