<?php

require_once("Animal.php");
class Frog extends Animal{
    public $legs = 4;
    public $cold_blooded = "No";

    public function lompat($jump){
        echo "Jump : " . $jump . "<br><br>";
    }
}

?>